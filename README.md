# 图标选择器 Plus

!> 这是本人最后一个项目了，本组织不再维护！感谢支持！

这是 图标选择器(https://gitee.com/layui-exts/icon-selected) 的升级版本，如果该插件暂时不能满足您的诉求，那么可以试试这一款新的扩展！

## 效果展示

![效果图](thumb.jpg)

## 应用场景

1. 比如后台管理系统，给菜单节点添加图标
2. 其他场景

## 兼容性

人类浏览器！不要问我什么不是人类浏览器，我是傻子，我不知道！

## 如何使用

1. 下载代码, 通常你可以在 [Releases](https://gitee.com/layui-exts/icon-selected-plus/releases) 页面下载到最新版额.
2. 解压扩展到您项目里面的扩展目录, 譬如: `libs/layui_exts`
3. 使用 `layui.config` 和 `layui.extend` 配置并注册扩展.
4. 使用 `layui.use` 来按需引用扩展并调用.

## 快速上手

### 配置并注册扩展

```javascript
// 配置扩展路径
layui.config({
    base: "../libs/layui_exts/",
});
// 注册扩展
layui.extend({
    iconSelectedPlus: "iconSelectedPlus/index",
});
```

### 引用并使用

```javascript
layui.use(["iconSelectedPlus"], function () {
    var iconSelectedPlus = layui.iconSelectedPlus;

    // 默认配置
    iconSelectedPlus.render(".icon-selected1");
});
```

### 可配置项目

这是目前插件支持的所有配置项，所有配置项都不是必须得，你根据自己的实际诉求选择自己需要的使用即可！

-   你可以通过阅读 `example/index.html` 来学习基本用法！
-   你可也可以访问： https://layui-exts.gitee.io/icon-selected-plus/example/ 查看效果
-   在线文档：https://layui-exts.gitee.io/icon-selected-plus/

```javascript
var opts = {
    // 默认提示文字
    placeholder: "选择图标",
    // 显示的图标，默认layui+fontAwesome4
    icons: [].concat(icons.layui, icons.fontAwesome4),
    // 选中图标后是否自动关闭弹层
    autoClose: true,
    // 是否是简约模式，简约模式就一个图标，没搜索用的输入框
    simple: false,
    // 图标容器的定位，默认下方，可设置为top
    position: "bottom",
    // 是否允许回车键快速选择第一个图标
    enterSelected: true,
    // 是否允许在按下esc后快速关闭容器
    escClose: true,
    // 事件托管
    event: {
        // DOM准备创建但还没有创建，虚晃一枪
        beforeCreate: function () {},
        // DOM已经创建但还没有塞到页面上，双喜临门！
        created: function () {},
        // DOM准备挂载了！但实际还没有挂载！三阳开泰！
        beforeMount: function () {},
        // DOM这回挂载了，简称：真实的DOM！nice！
        mounted: function () {},
        // 当某个图标被选择
        selected: function () {},
        // 当点击清理按钮
        clear: function () {},
    },
};
```
